# Car Rear LEDs
##Overview
This is an arduino project controlling 2 strips of WS2812b LEDs using an opto-isolator breakout board with 4 isolators for 2 signals, brake and reverse. The code senses the controls using quick connect taps on the rear lights. Onces sensed it will control the leds in sync with the tail lights but with cool bright animations.

Dev work YouTube video: 