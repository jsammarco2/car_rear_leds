#include "FastLED.h"

FASTLED_USING_NAMESPACE
#if FASTLED_VERSION < 3001000
#error "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DEBUG true
#define NUM_STRIPS 2
#define NUM_LEDS_PER_STRIP 18
CRGB leds[NUM_STRIPS][NUM_LEDS_PER_STRIP];

//#define DATA_PIN    6
//#define CLK_PIN   4
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS    18
#define BRIGHTNESS          100
#define FRAMES_PER_SECOND  120
#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))
//CRGB leds[NUM_LEDS];

const int leftLedPin = 6;
const int rightLedPin = 5;

const int reverseInPin = 2;
const int brakeInPin = 4;
const int leftInPin = 8;
const int rightInPin = 7;

int brakeOn = 0;
int rightOn = 0;
int leftOn = 0;
int reverseOn = 0;
bool brakePressed = false;
bool rightPressed = false;
bool leftPressed = false;
bool reversePressed = false;
bool startup = true;

void setup() {
  if(DEBUG){ Serial.begin(9600); }
  delay(100);
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,leftLedPin,COLOR_ORDER>(leds[0], NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
  FastLED.addLeds<LED_TYPE,rightLedPin,COLOR_ORDER>(leds[1], NUM_LEDS_PER_STRIP).setCorrection(TypicalLEDStrip);
  //FastLED.addLeds<LED_TYPE,DATA_PIN,CLK_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);
  fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black); // Set all to black.
  fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Black); // Set all to black.
  FastLED.show();
  pinMode(brakeInPin, INPUT);
  pinMode(rightInPin, INPUT);
  pinMode(leftInPin, INPUT);
  pinMode(reverseInPin, INPUT);
}

typedef void (*SimplePatternList[])();
SimplePatternList brakePattern = { sinelon, flicker, solid };
SimplePatternList rightPattern = { right };
SimplePatternList leftPattern = { left };
uint8_t gCurrentBrakePatternNumber = 0;
uint8_t gCurrentRightPatternNumber = 0;
uint8_t gCurrentLeftPatternNumber = 0;
uint8_t gCurrentReversePatternNumber = 0;
uint8_t gHue = 0; // rotating "base color" used by many of the patterns
uint8_t gRightLedPos = 2;
uint8_t gLeftLedPos = 2;
int hazardsOffFor = 2000;
int hazardsOnFor = 0;

void loop() {
  if(startup){
    startup = false;
    myRainbow(240, 5);
    drivingLights();
  }
  
  brakeOn = digitalRead(brakeInPin);
  rightOn = digitalRead(rightInPin);
  leftOn = digitalRead(leftInPin);
  reverseOn = digitalRead(reverseInPin);
  if(rightOn == HIGH){ rightPressed = true; }else{ rightPressed = false; }
  if(leftOn == HIGH){ leftPressed = true; }else{ leftPressed = false; }
  if(brakeOn == HIGH){ brakePressed = true; }else{ brakePressed = false; }
  if(reverseOn == HIGH){ reversePressed = true; }else{ reversePressed = false; }
  
  if(brakePressed){
    if(gCurrentBrakePatternNumber < ARRAY_SIZE(brakePattern)){
      brakePattern[gCurrentBrakePatternNumber]();
    }else{
      if(rightOn || leftOn){
        //EVERY_N_MILLISECONDS(20) { solid(); }
        solid();
      }
    }
  }else{
    gCurrentBrakePatternNumber = 0;
  }
  if(rightPressed){
    rightPattern[gCurrentRightPatternNumber]();
  }
  if(leftPressed){
    leftPattern[gCurrentLeftPatternNumber]();
  }
  if(reversePressed){
    reversePattern();
  }
  if(rightPressed && leftPressed){
    //if(DEBUG){ Serial.print("Hazards Off For: "); Serial.print(hazardsOffFor); Serial.print(" On For: "); Serial.println(hazardsOnFor); }
    hazardsOffFor = 0;
    if(hazardsOnFor < 1000) { hazardsOnFor++; }//Frames
  }else{
    hazardsOnFor = 0;
    if(hazardsOffFor < 1000) { hazardsOffFor++; }//Frames
  }
  if(hazardsOffFor < 2 && hazardsOnFor < 2){
    myRainbow(120, 1);//60 = Frames = 1 Seconds
  }
  if(!leftPressed && !rightPressed && !brakePressed && !reversePressed && !startup){
    drivingLights();
    if(DEBUG){ Serial.println("ALL OFF"); }
  }else{
    if(DEBUG){ Serial.print("<:"); Serial.print(leftOn); Serial.print(" R:"); Serial.print(reverseOn); Serial.print(" B:"); Serial.print(brakeOn); Serial.print(" >:"); Serial.println(rightOn); }
  }
  FastLED.show();
  FastLED.delay(1000/FRAMES_PER_SECOND);
  EVERY_N_MILLISECONDS( 10 ) { gHue++; }
  EVERY_N_MILLISECONDS( 400 ) { nextPattern(); }
  delay(1);//GIVE IT A MILLI, A MILLI, A MILLI
}

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentRightPatternNumber = (gCurrentRightPatternNumber + 1) % ARRAY_SIZE( rightPattern);
  gCurrentLeftPatternNumber = (gCurrentLeftPatternNumber + 1) % ARRAY_SIZE( leftPattern);
  if(gCurrentBrakePatternNumber < ARRAY_SIZE(brakePattern)){
    gCurrentBrakePatternNumber = (gCurrentBrakePatternNumber + 1);
  }
}

void drivingLights()
{
  fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Maroon);
  fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Maroon);
  fadeToBlackBy(leds[0], NUM_LEDS_PER_STRIP, 200);
  fadeToBlackBy(leds[1], NUM_LEDS_PER_STRIP, 200);
}

void myRainbow(int duration, int speed)
{
  for( int i = 0; i < duration; i++) {
    rainbow();
    FastLED.show();
    EVERY_N_MILLISECONDS( 10 ) { gHue += speed; }
    FastLED.delay(1000/FRAMES_PER_SECOND);
  }
}

void reversePattern()
{
  fadeToBlackBy(leds[0], NUM_LEDS_PER_STRIP, 50);
  fadeToBlackBy(leds[1], NUM_LEDS_PER_STRIP, 50);
  int pos = beatsin16(40,0,NUM_LEDS_PER_STRIP);
  leds[0][pos] += CRGB::Azure;
  leds[1][pos] += CRGB::Azure;
}

void rainbow() 
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds[0], NUM_LEDS_PER_STRIP, gHue, 7);
  fill_rainbow( leds[1], NUM_LEDS_PER_STRIP, gHue, 7);
}

void rainbowWithGlitter() 
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter) 
{
  if( random8() < chanceOfGlitter) {
    leds[0][ random16(NUM_LEDS_PER_STRIP) ] += CRGB::Red;
  }
}

void confetti() 
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds[0], NUM_LEDS_PER_STRIP, 10);
  int pos = random16(NUM_LEDS_PER_STRIP);
  leds[0][pos] += CHSV( gHue + random8(64), 255, 255);
}

void sinelon()
{
  EVERY_N_MILLISECONDS( 10 ) {
    // a colored dot sweeping back and forth, with fading trails
    fadeToBlackBy(leds[0], NUM_LEDS_PER_STRIP, 100);
    fadeToBlackBy(leds[1], NUM_LEDS_PER_STRIP, 100);
    int pos = beatsin16(150,0,NUM_LEDS_PER_STRIP);
    //Serial.println(pos);
    //leds[pos] += CHSV(0, 255, 255);
    leds[0][pos] += CRGB::Maroon;
    leds[1][pos] += CRGB::Maroon;
    //int myPos = random16(NUM_LEDS);
    //leds[myPos] -= CHSV(150, 0, 0);
    //leds[myPos] += CHSV(random8(64), random8(32), random8(32));
  }
}

void right()
{
  EVERY_N_MILLISECONDS( 30 ) {
    fadeToBlackBy(leds[1], NUM_LEDS_PER_STRIP, 100);
    //leds[pos] += CHSV(0, 255, 255);
    leds[1][gRightLedPos] += CRGB::Orange;
    leds[1][gRightLedPos-1] += CRGB::Orange;
    leds[1][gRightLedPos-2] += CRGB::Orange;
    gRightLedPos += 1;
    if (gRightLedPos >= NUM_LEDS_PER_STRIP) { gRightLedPos = 2; }
  }
}

void left()
{
  EVERY_N_MILLISECONDS( 30 ) {
    fadeToBlackBy(leds[0], NUM_LEDS_PER_STRIP, 100);
    //leds[pos] += CHSV(0, 255, 255);
    leds[0][gLeftLedPos] += CRGB::Orange;
    leds[0][gLeftLedPos-1] += CRGB::Orange;
    leds[0][gLeftLedPos-2] += CRGB::Orange;
    gLeftLedPos += 1;
    if (gLeftLedPos >= NUM_LEDS_PER_STRIP) { gLeftLedPos = 2; }
  }
}

void flicker() 
{
  fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Black); // Set all to black.
  fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Black); // Set all to black.
  EVERY_N_MILLISECONDS( 50 ) {
    //fill_solid(leds, NUM_LEDS, CRGB::Red); // Set all to red.
    fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Maroon);
    fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Maroon);
  }
}

void solid() 
{
  //fill_solid(leds, NUM_LEDS, CRGB::Red); // Set all to red.
  fill_solid(leds[0], NUM_LEDS_PER_STRIP, CRGB::Maroon);
  fill_solid(leds[1], NUM_LEDS_PER_STRIP, CRGB::Maroon);
  //FastLED.show();
  //delay(1000);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 62;
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for( int i = 0; i < NUM_LEDS_PER_STRIP; i++) { //9948
    leds[0][i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
    leds[1][i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
  }
}

void juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds[0], NUM_LEDS_PER_STRIP, 20);
  fadeToBlackBy( leds[1], NUM_LEDS_PER_STRIP, 20);
  byte dothue = 0;
  for( int i = 0; i < 8; i++) {
    leds[0][beatsin16(i+7,0,NUM_LEDS_PER_STRIP)] |= CHSV(dothue, 200, 255);
    leds[1][beatsin16(i+7,0,NUM_LEDS_PER_STRIP)] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

